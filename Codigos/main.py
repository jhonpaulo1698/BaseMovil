"""
Autor: José Luis Laica, Luis Moyón, Steven Silva, Óscar Ávila, Dennys Paillacho
Organización: FunPython
01/09/2017
Guayaquil - Ecuador
Bajo licencia CC BY-SA 4.0
Esta licencia le permite a otras personas remezclar, retocar y construir sobre su trabajo incluso para propósitos comerciales, 
siempre y cuando le den crédito y licencien sus nuevas creaciones bajo términos idénticos. 
"""

#programado en la version 1.8.7 de MicroPython
#puede que en otras versiones no esten implementados
#los metodos incluidos en este codigo

#librerias usadas para controlar los pines, la red WiFi
#los puertos y tiempos
from machine import UART
import network
import socket
import time

#response() funcion utilizada para agarrar los
#datos enviados por el codigo externo.
def response(connfd):
    while True:
        bytesRecv = connfd.recv(1024)
        if bytesRecv != None:
            #decode necesario para poder luego enviar los datos
            #por uart.
            strRecv = bytesRecv.decode()
            uart.write(strRecv)
            #send() envia los datos por el tx del esp.
            connfd.send(repns)
    
uart = UART(0, 115200)
uart.init(115200, bits=8, parity=None, stop=1)

#Se habilita la conexion wifi para el esp.
nic = network.WLAN(network.STA_IF)
nic.active(True)
#Accion para hacer si el esp no esta conectado a una red.
if not nic.isconnected():
    #connect(), nos ayuda a conectarnos, se especifica
    #nombre de red y clave.
    nic.connect('SSID', 'PASSWORD')
    while nic.isconnected():
            pass

time.sleep(10)
print(nic.isconnected())
print(nic.ifconfig())

#socket necesario para que el esp escuche
#datos enviados por un codigo externo.
s = socket.socket()
#bind() ayuda a conectarse al puerto del codigo
#por el que se envian los datos.
s.bind(("",123))
#listen() es el metodo para escuchar los datos.
s.listen(1)

while True:
    cl, addr = s.accept()
    response(cl) #funcion para decidir que hacer con la nueva conexion
    cl.close()
